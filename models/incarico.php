<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_jooincarichi
 *
 * @author      Dario Casertano - www.casertano.name
 * @copyright   Copyright (c) 2016 Casertano Dario - All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported.
 *
 * @version     16.6.1
 */
defined('_JEXEC') or die;

use Joomla\String\StringHelper;
use Joomla\Registry\Registry as JRegistry;

class JooIncarichiModelIncarico extends JModelAdmin
{
	protected $text_prefix = 'COM_JOOINCARICHI';

	/**
	 * @inheritdoc
	 *
	 * @param string $type
	 * @param string $prefix
	 * @param array $config
	 *
	 * @return JTable
	 *
	 * @since 12.2
	 */
	public function getTable ( $type = 'JooIncarichi', $prefix = 'JTable', $config = array() )
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * @inheritdoc
	 *
	 * @param array $data
	 *
	 * @return bool
	 *
	 * @since 12.2
	 */
	public function save ( $data )
	{
		/**
		 * @var JApplication
		 */
		$JApplication = JFactory::getApplication();

		$data['alias'] = JFilterOutput::stringURLSafe($data['title']);

		list($data['introtext'], $data['fulltext']) = array_map('trim', explode('<hr id="system-readmore" />', $data['itemtext']));
		unSet($data['itemtext']);

		$data['created'] = empty($data['created']) ? date('Y-m-d H:i:s') : $data['created'];
		if (empty($data['created_by']))
		{
            /** @var JUser $JUser */
			$JUser = JFactory::getUser();
			$data['created_by'] = $JUser->get('id');
		}

        $data['publish_up'] = empty($data['publish_up']) ? date('Y-m-d H:i:s') : $data['publish_up'];

		if ($JApplication->input->get('task') == 'save2copy')
		{
			list($data['title'], $data['alias']) = $this->generateNewTitle($data['alias'], $data['title']);
			$data['state'] = 0;
		}

        unSet($data['tags']);

        return parent::save($data);
	}

	/**
	 * @inheritdoc
	 *
	 * @param array $pks
	 * @param int $state
	 *
	 * @return bool
	 *
	 * @since 12.2
	 */
	public function publish ( &$pks, $state = 1 )
	{
		JPluginHelper::importPlugin('content');

        /** @var JTable $JTable */
		$JTable = $this->getTable();

		foreach ($pks as $i => $pk)
		{
			$JTable->reset();

			if ($JTable->load($pk))
			{
				if (!$this->canEditState($JTable))
				{
					unSet($pks[$i]);
                    throw new Exception(JText::_('JLIB_APPLICATION_ERROR_EDITSTATE_NOT_PERMITTED'), 403);
				}
				else
				{
					if (!$JTable->bind(array('state' => $state))) return false;
					if (!$JTable->check()) return false;
					if (!$JTable->store()) return false;
				}
			}
		}

		$JEventDispatcher = JEventDispatcher::getInstance();
		$result = $JEventDispatcher->trigger('onContentChangeState', array("{$this->option}.{$this->name}", $pks));

		if (in_array(false, $result, true)) return false;

		return true;
	}

	/**
	 * @inheritdoc
	 *
	 * @param array &$pks
	 *
	 * @return bool
	 *
	 * @since 12.2
	 */
	public function delete ( &$pks )
	{
		if (parent::delete($pks))
		{
			foreach ($pks as $pk) $this->deleteAttachments($pk);

            return true;
		}
        else return false;
	}

	/**
	 * @param int $pk
	 *
	 * @return bool
	 *
	 * @since 12.2
	 */
	public function deleteAttachments ( $pk )
	{
        /** @var JDatabaseDriver $JDbo */
		$JDbo = $this->getDbo();

        /** @var JDatabaseQuery $delete */
		$delete = $JDbo->getQuery(true);
		$delete->delete('#__jooincarichi_attachments')
			->where("(incarico_id = {$pk})");

		if ($JDbo->setQuery($delete)->execute())
		{
			$params = JComponentHelper::getParams('com_jooincarichi');
			$basepath = JPATH_SITE . '/' . $params->get('file_path', 'images/jooincarichi') . "/{$pk}";

			if (file_exists($basepath))
			{
				$dir = new DirectoryIterator($basepath);
				foreach ($dir as $item)
					if (!$item->isDot() && $item->isFile())
						@unlink($item->getPathname());

				return @rmdir($basepath);
			}
			else return true;
		}
		else return true;
	}

	/**
	 * @inheritdoc
	 *
	 * @param mixed $data
	 * @param mixed $loadData
	 *
	 * @return JForm
	 *
	 * @since 12.2
	 */
	public function getForm ( $data = array(), $loadData = true )
	{
        /** @var JForm $JForm */
		$JForm = $this->loadForm('com_jooincarichi.incarico', 'incarico', array('control' => 'jform', 'load_data' => $loadData));
        return empty($JForm) ? false : $JForm;
	}

	/**
	 * @inheritdoc
	 *
	 * @param int $pk
	 *
	 * @return JObject
	 *
	 * @since 12.2
	 */
	public function getIncarico ( $pk = Null )
	{
		/**
		 * @var JObject
		 */
		if ($incarico = parent::getItem($pk))
		{
			$JRegistry = new JRegistry();
			$JRegistry->loadString($incarico->get('metadata'));
			$incarico->set('metadata', $JRegistry->toArray());
			$incarico->set('itemtext', $incarico->get('fulltext', false) ? $incarico->get('introtext') . '<hr id="system-readmore" />' . $incarico->get('fulltext') : $incarico->get('introtext'));
		}

		return $incarico;
	}

	/**
	 * @param int $pk
	 *
	 * @return array
	 *
	 * @since 12.2
	 */
	public function getAttachments ( $pk = Null )
	{
		$pk = !empty($pk) ? $pk : (int) $this->getState($this->getName() . '.id');

		$dbo = $this->getDbo();
		{
			$query = $dbo->getQuery(true);
			$query->from('#__jooincarichi_attachments')
                ->select('*')
				->where("(item_id = {$pk})")
				->order('ordering');
		}
		return $dbo->setQuery($query)->loadObjectList();
	}

	/**
	 * @return JObject
	 *
     * @version 16.6.6
     *
	 * @since 12.2
	 */
	protected function loadFormData()
	{
		$data = JFactory::getApplication()->getUserState('com_jooincarichi.edit.incarico.data', array());
		if (empty($data)) $data = $this->getIncarico();

		return $data;
	}

	/**
	 * @inheritdoc
	 *
	 * @param JTable $JTable
	 *
	 * @return void
	 *
     * @version 16.6.6
     *
	 * @since 12.2
	 */
	protected function prepareTable ( $JTable )
	{
		if (($JTable->state == 1) && !intval($JTable->publish_up)) $JTable->publish_up = JFactory::getDate()->toSql();
		$JTable->version++;

		if (empty($JTable->id)) $JTable->reorder('(state >= 0)');
	}

	/**
	 * @inheritdoc
	 *
	 * @param object $record
	 *
	 * @return bool
	 *
	 * @since 12.2
	 */
	protected function canDelete ( $record )
	{
		if (!empty($record->id))
		{
			if ($record->state != -2) return;

			return JFactory::getUser()->authorise('core.delete', 'com_jooincarichi.incarico.' . (int) $record->id);
		}
	}

	/**
	 * @inheritdoc
	 *
	 * @param object $record
	 *
	 * @return bool
	 *
	 * @since 12.2
	 */
	protected function canEditState ( $record )
	{
		return JFactory::getUser()->authorise('core.edit.state', $this->option);
	}

    /**
     * @inheritdoc
     *
     * @param string $alias The alias.
     * @param string $title The title.
     *
     * @return array
	 *
	 * @since 12.2
     */
    protected function generateNewTitle ( $alias, $title )
    {
        /** @var JTable $JTable */
        $JTable = $this->getTable();

        while ($JTable->load(array('alias' => $alias)))
        {
            $title = StringHelper::increment($title);
            $alias = StringHelper::increment($alias, 'dash');
        }

        return array($title, $alias);
    }
}
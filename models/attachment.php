<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_jooincarichi
 *
 * @author      Dario Casertano - www.casertano.name
 * @copyright   Copyright (c) 2016 Casertano Dario - All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported.
 *
 * @version     16.6.6
 */
defined('_JEXEC') or die;

use Joomla\Registry\Registry as JRegistry;

class JooIncarichiModelAttachment extends JModelAdmin
{
	/**
	 * @inheritdoc
	 *
	 * @param string $type
	 * @param string $prefix
	 * @param array $config
	 *
	 * @return JTable
	 *
	 * @since 12.2
	 */
	public function getTable ( $type = 'JooIncarichiAttachments', $prefix = 'JTable', $config = array() )
	{
		return JTable::getInstance($type, $prefix, $config);
	}

    /**
     * @inheritdoc
     *
     * @param JInput $__POST
     *
     * @return array
     *
     * @since 12.2
     */
    public function delete ( $__POST )
    {
        /** @var JRegistry $JooIncarichiParams */
        $JooIncarichiParams = JComponentHelper::getParams('com_jooincarichi');

        $file_path = JPATH_SITE . '/' . $JooIncarichiParams->get('file_path', 'images/jooincarichi') . '/' . $__POST->getUint('item_id') . '/' . $__POST->get('name');

        try
        {
            /** @var JTable $JTable */
            $JTable = $this->getTable();

            if (!$JTable->delete($__POST->getUint('id'))) throw new Exception('JTable::delete', 500);
            else unlink($file_path);

            return array
            (
                'returnCode' => 200,
            );
        }
        catch (Exception $e)
        {
            return array
            (
                'returnCode' => $e->getCode(),
                'returnMessage' => $e->getMessage(),
                'returnFile' => $e->getFile(),
                'returnLine' => $e->getLine(),
            );
        }
    }

    /**
     * @inheritdoc
     *
     * @param JInput $__POST
     *
     * @return array
     *
     * @since 12.2
     */
    public function ordering ( $__POST )
    {
        try
        {
            /** @var JTable $JTable */
            $JTable = $this->getTable();

            foreach ($__POST->get('ids', null, 'array') as $order => $id)
            {
                $JTable->load($id);
                if (!$JTable->bind(array
                (
                    'ordering' => $order,
                ))) throw new Exception('JTable::bind', 500);
                if (!$JTable->check()) throw new Exception('JTable::check', 500);
                if (!$JTable->store()) throw new Exception('JTable::store', 500);

                $this->cleanCache();

                $JTable->reset();
            }

            return array
            (
                'returnCode' => 200,
            );
        }
        catch (Exception $e)
        {
            return array
            (
                'returnCode' => $e->getCode(),
                'returnMessage' => $e->getMessage(),
                'returnFile' => $e->getFile(),
                'returnLine' => $e->getLine(),
            );
        }
    }

	/**
	 * @inheritdoc
	 *
	 * @param JInput $__POST
	 * @param JInputFiles $__FILES
	 *
	 * @return array
	 *
	 * @since 12.2
	 */
	public function save ( $__POST, $__FILES )
	{
	    $data = array();

	    /** @var JRegistry $JooIncarichiParams */
        $JooIncarichiParams = JComponentHelper::getParams('com_jooincarichi');

        /** @var array $FILES */
        $FILES = $__FILES->get('upload', array(), 'array');

        $file_path = JPATH_SITE . '/' . $JooIncarichiParams->get('file_path', 'images/jooincarichi') . '/' . $__POST->getUint('item_id');
        if (!file_exists($file_path)) mkdir($file_path, 0755, true);

        $file_name_normalized = JFilterOutput::stringURLSafe(pathinfo($__POST->get('_upload'), PATHINFO_FILENAME)) . '.' . pathinfo($__POST->get('_upload'), PATHINFO_EXTENSION);

        $file_name = "{$file_path}/{$file_name_normalized}";

        while (file_exists($file_name))
        {
            $file_name_normalized = rand(0, getrandmax()) . "_{$file_name_normalized}";
            $file_name = "{$file_path}/{$file_name_normalized}";
        }

        $data['item_id'] = $__POST->getUint('item_id');
        $data['media_path'] = $file_name_normalized;
        $data['title'] = ucwords(str_replace(array('_', '-'), ' ', pathinfo($__POST->get('_upload'), PATHINFO_FILENAME)));
        $data['alias'] = JFilterOutput::stringURLSafe($data['title']);
        $data['created'] = date('Y-m-d H:i:s');
        $data['created_by'] = $__POST->getUint('created_by');

        if (@move_uploaded_file($FILES['tmp_name'], $file_name))
        {
            chmod($file_name, 0644);

            try
            {
                /** @var JTable $JTable */
                $JTable = $this->getTable();

                /** @var string $key */
                $key = $JTable->getKeyName();

                if (!$JTable->bind($data)) throw new Exception('JTable::bind', 500);
                if (!$JTable->check()) throw new Exception('JTable::check', 500);
                if (!$JTable->store()) throw new Exception('JTable::store', 500);

                $this->cleanCache();

                return array
                (
                    'returnCode' => 200,
                    'id' => $JTable->{$key},
                    'file_name' => $data['media_path'],
                    'title' => $data['title'],
                );
            }
            catch (Exception $e)
            {
                return array
                (
                    'returnCode' => $e->getCode(),
                    'returnMessage' => $e->getMessage(),
                    'returnFile' => $e->getFile(),
                    'returnLine' => $e->getLine(),
                );
            }
        }
        else throw new Exception('move_uploaded_file', 500);
	}

	/**
	 * @inheritdoc
	 *
	 * @param JInput $__POST
	 *
	 * @return array
	 *
	 * @since 12.2
	 */
	public function state ( $__POST )
	{
        try
        {
            /** @var JTable $JTable */
            $JTable = $this->getTable();
            $JTable->reset();
            $JTable->load($__POST->getUint('id'));

            if (!$JTable->bind(array
            (
                'state' => $__POST->get('state'),
                'modified' => date('Y-m-d H:i:s'),
                'modified_by' => $__POST->getUint('modified_by'),
            ))) throw new Exception('JTable::bind', 500);

            if (!$JTable->check()) throw new Exception('JTable::check', 500);
            if (!$JTable->store()) throw new Exception('JTable::store', 500);

            return array
            (
                'returnCode' => 200,
            );
        }
        catch (Exception $e)
        {
            return array
            (
                'returnCode' => $e->getCode(),
                'returnMessage' => $e->getMessage(),
                'returnFile' => $e->getFile(),
                'returnLine' => $e->getLine(),
            );
        }
	}

    /**
     * @inheritdoc
     *
     * @param JInput $__POST
     *
     * @return array
     *
     * @since 12.2
     */
    public function update ( $__POST )
    {
        try
        {
            /** @var JTable $JTable */
            $JTable = $this->getTable();
            $JTable->reset();
            $JTable->load($__POST->getUint('id'));

            if (!$JTable->bind(array
            (
                'title' => $__POST->get('title', null, 'raw'),
                'alias' => JFilterOutput::stringURLSafe($__POST->get('title', null, 'raw')),
                'modified' => date('Y-m-d H:i:s'),
                'modified_by' => $__POST->getUint('modified_by'),
            ))) throw new Exception('JTable::bind', 500);

            if (!$JTable->check()) throw new Exception('JTable::check', 500);
            if (!$JTable->store()) throw new Exception('JTable::store', 500);

            return array
            (
                'returnCode' => 200,
            );
        }
        catch (Exception $e)
        {
            return array
            (
                'returnCode' => $e->getCode(),
                'returnMessage' => $e->getMessage(),
                'returnFile' => $e->getFile(),
                'returnLine' => $e->getLine(),
            );
        }
    }

    public function getForm ( $data = array(), $loadData = true )
    {
	}
}
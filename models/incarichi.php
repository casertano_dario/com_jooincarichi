<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_JooIncarichi
 *
 * @author      Dario Casertano - www.casertano.name
 * @copyright   Copyright (c) 2016 Casertano Dario - All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported.
 * @version     16.5.31
 */
defined('_JEXEC') or die;

class JooIncarichiModelIncarichi extends JModelList
{
    /**
     * @inheritdoc
     *
     * @param string $type
     * @param string $prefix
     * @param array $config
     *
     * @return JTable
     *
     * @since 12.2
     */
    public function getTable ( $type = 'JooIncarichi', $prefix = 'JTable', $config = array() )
    {
        return JTable::getInstance($type, $prefix, $config);
    }

    /**
     * @inheritdoc
     *
     * @return array|false
     *
     * @since 12.2
     */
    public function getIncarichi ()
    {
        return parent::getItems();
    }

    /**
     * The list of Authors
     *
     * @return array|false
     *
     * @since 12.2
     */
    public function getAuthors ()
    {
        /** @var JDatabaseDriver $JDbo */
        $JDbo = $this->getDbo();

        /** @var JDatabaseQuery $select */
        $select = $JDbo->getQuery(true)
            ->from('#__users AS u')
            ->select('u.id AS value')
            ->select('u.name AS text')
            ->join('INNER', '#__jooincarichi AS ji ON (ji.created_by = u.id)')
            ->group('u.id, u.name')
            ->order('u.name');

        return $JDbo->setQuery($select)->loadObjectList();
    }

    /**
     * Lista "Tipo incarico"
     *
     * @return array
     *
     * @since 12.2
     *
     * @version 16.6.7
     */
    public function getTipo_incarico ()
    {
        return array
        (
            JText::_('COM_JOOINCARICHI_FIELD_TIPO_INCARICO_OPTION_0_LABEL'),
            JText::_('COM_JOOINCARICHI_FIELD_TIPO_INCARICO_OPTION_1_LABEL'),
            JText::_('COM_JOOINCARICHI_FIELD_TIPO_INCARICO_OPTION_2_LABEL'),
        );
    }

    /**
     * @inheritdoc
     *
     * @param string $ordering
     * @param string $direction
     *
     * @since 12.2
     *
     * @version 16.6.7
     */
    protected function populateState ( $ordering = 'ji.title', $direction = 'ASC' )
    {
        $app = JFactory::getApplication();

        if ($layout = $app->input->get('layout')) $this->context .= '.' . $layout;

        $search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
        $this->setState('filter.search', $search);

        $access = $this->getUserStateFromRequest($this->context . '.filter.access', 'filter_access', 0, 'int');
        $this->setState('filter.access', $access);

        $authorId = $app->getUserStateFromRequest($this->context . '.filter.author_id', 'filter_author_id');
        $this->setState('filter.author_id', $authorId);

        $published = $this->getUserStateFromRequest($this->context . '.filter.published', 'filter_published', '');
        $this->setState('filter.published', $published);

        $language = $this->getUserStateFromRequest($this->context . '.filter.language', 'filter_language', '');
        $this->setState('filter.language', $language);

        $tipo_incarico = $this->getUserStateFromRequest($this->context . '.filter.tipo_incarico', 'filter_tipo_incarico', '');
        $this->setState('filter.tipo_incarico', $tipo_incarico);

        $forcedLanguage = $app->input->get('forcedLanguage');
        if (!empty($forcedLanguage))
        {
            $this->setState('filter.language', $forcedLanguage);
            $this->setState('filter.forcedLanguage', $forcedLanguage);
        }

        $tag = $this->getUserStateFromRequest($this->context . '.filter.tag', 'filter_tag', '');
        $this->setState('filter.tag', $tag);

        parent::populateState($ordering, $direction);
    }

    /**
     * @inheritdoc
     *
     * @since 12.2
     *
     * @version 16.6.7
     */
    protected function getListQuery ()
    {
        /** @var JUser $JUser */
        $JUser = JFactory::getUser();

        /** @var JDatabaseDriver $JDbo */
        $JDbo = $this->getDbo();

        /** @var JDatabaseQuery $select */
        $select = $JDbo->getQuery(true)
            ->from('#__jooincarichi AS ji')
            ->select('ji.id')
            ->select('ji.title')
            ->select('ji.alias')
            ->select('ji.tipo_incarico')
            ->select('ji.state')
            ->select('ji.created')
            ->select('ji.created_by')
            ->select('ji.created_by_alias')
            ->select('ji.language')
            ->select('ji.ordering')
            ->select('ji.publish_up');

        $select->join('LEFT', '#__languages AS l ON (l.lang_code = ji.language)')
            ->select('l.title AS language_title');

        $select->join('LEFT', '#__viewlevels AS ag ON (ag.id = ji.access)')
            ->select('ag.title AS access_level');

        $select->join('LEFT', '#__users AS ua ON (ua.id = ji.created_by)')
            ->select('ua.name AS author_name');

        if ($access = $this->getState('filter.access')) $select->where('(ji.access = ' . (int) $access . ')');

        if ($tipo_incarico = $this->getState('filter.tipo_incarico')) $select->where('(ji.tipo_incarico = ' . (int) $tipo_incarico . ')');

        if (!$JUser->authorise('core.admin')) $select->where('(ji.access IN (' . implode(',', $JUser->getAuthorisedViewLevels()) . '))');

        $published = $this->getState('filter.published');
        if (is_numeric($published)) $select->where('(ji.state = ' . (int) $published . ')');
        elseif ($published === '') $select->where('(ji.state IN (0, 1))');

        $authorId = $this->getState('filter.author_id');
        if (is_numeric($authorId))
        {
            $type = $this->getState('filter.author_id.include', true) ? '=' : '<>';
            $select->where("(ji.created_by {$type} " . (int) $authorId . ')');
        }

        $search = $this->getState('filter.search');
        if (!empty($search))
        {
            if (stripos($search, 'id:') === 0) $select->where('a.id = ' . (int)substr($search, 3));
            elseif (stripos($search, 'author:') === 0)
            {
                $search = $JDbo->quote('%' . $JDbo->escape(substr($search, 7), true) . '%');
                $select->where("(ua.name LIKE {$search} OR ua.username LIKE {$search})");
            }
            else
            {
                $search = $JDbo->quote('%' . $JDbo->escape($search, true) . '%');
                $select->where("(ji.title LIKE {$search} OR ji.alias LIKE {$search})");
            }
        }

        if ($language = $this->getState('filter.language')) $select->where('(ji.language = ' . $JDbo->quote($language) . ')');

        $orderCol = $this->getState('list.ordering', 'ji.title');
        $orderDirn = $this->getState('list.direction', 'asc');
        if ($orderCol == 'language') $orderCol = 'l.title';
        if ($orderCol == 'access_level') $orderCol = 'ag.title';

        return $select->order("{$orderCol} {$orderDirn}");
    }
}

<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_jooincarichi
 *
 * @author      Dario Casertano - www.casertano.name
 * @copyright   Copyright (c) 2016 Casertano Dario - All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported.
 * @version     16.5.31
 */
defined('_JEXEC') or die;

class JooIncarichiHelper
{
    /**
     * Gets a list of the actions that can be performed.
     *
     * @param int $itemId
     *
     * @return JObject
     *
     * @since 1.6
     */
    public static function getActions ( $itemId = 0 )
    {
        /** @var JUser $JUser */
        $JUser = JFactory::getUser();

        $JObject = new JObject();

        $assetName = $itemId ? 'com_jooproducts.item.' . (int) $itemId : 'com_jooproducts';

        foreach (array
                 (
                     'core.admin',
                     'core.manage',
                     'core.create',
                     'core.edit',
                     'core.edit.own',
                     'core.edit.state',
                     'core.delete',
                 ) as $action) $JObject->set($action, $JUser->authorise($action, $assetName));

        return $JObject;
    }
}

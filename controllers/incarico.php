<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_jooincarichi
 *
 * @author      Dario Casertano - www.casertano.name
 * @copyright   Copyright (c) 2016 Casertano Dario - All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported.
 * @version     16.6.7
 */
defined('_JEXEC') or die;

class JooIncarichiControllerIncarico extends JControllerForm
{
    /**
     * Constructor.
     *
     * @param array $config
     *
     * @since 12.2
     *
     * @throws  Exception
     */
    public function __construct ( $config = array() )
    {
        parent::__construct($config);

        if ($this->view_list == 'incaricos') $this->view_list = 'incarichi';
    }

    /**
     * @inheritdoc
     *
     * @param   array $data An array of input data.
     * @param   string $key The name of the key for the primary key.
     *
     * @return  boolean
     *
     * @since 12.2
     */
    protected function allowEdit ( $data = array(), $key = 'id' )
    {
        /** @var JUser $JUser */
        $JUser = JFactory::getUser();

        $recordId = (int) isSet($data[$key]) ? $data[$key] : 0;

        if ($JUser->authorise('core.edit', "com_jooincarichi.item.{$recordId}")) return true;
        elseif ($JUser->authorise('core.edit.own', "com_jooincarichi.item.{$recordId}"))
        {
            $ownerId = (int) isSet($data['created_by']) ? $data['created_by'] : 0;
            if (empty($ownerId) && $recordId)
            {
                $record = $this->getModel()->getItem($recordId);
                if (empty($record)) return false;
                $ownerId = $record->created_by;
            }

            if ($ownerId == $JUser->get('id')) return true;
        }

        return parent::allowEdit($data, $key);
    }
}

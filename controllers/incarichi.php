<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_jooincarichi
 *
 * @author      Dario Casertano - www.casertano.name
 * @copyright   Copyright (c) 2016 Casertano Dario - All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported.
 * @version     16.5.31
 */
defined('_JEXEC') or die;

class JooIncarichiControllerIncarichi extends JControllerAdmin
{
	/**
	 * @inheritdoc
	 *
	 * @param mixed $name
	 * @param mixed $prefix
	 *
	 * @return object
	 *
	 * @since 12.2
	 */
	public function getModel ( $name = 'Incarico', $prefix = 'JooIncarichiModel' )
	{
		return parent::getModel($name, $prefix, array('ignore_request' => true));
	}
}
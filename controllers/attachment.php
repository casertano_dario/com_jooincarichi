<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_jooincarichi
 *
 * @author      Dario Casertano - www.casertano.name
 * @copyright   Copyright (c) 2016 Casertano Dario - All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported.
 *
 * @version     16.6.6
 */
defined('_JEXEC') or die;

class JooIncarichiControllerAttachment extends JControllerForm
{
    /**
     * @since 12.2
     */
    public function delete ()
    {
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /** @var JInput $JInput */
        $JInput = $this->input;

        /** @var JooIncarichiModelAttachment $JModel */
        $JModel = $this->getModel();

        /** @var array $result */
        if ($result = $JModel->delete($JInput->post))
        {
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($result);
            exit;
        }
    }

    /**
     * @since 12.2
     */
    public function ordering ()
    {
        /** @var JInput $JInput */
        $JInput = $this->input;

        /** @var JooIncarichiModelAttachment $JModel */
        $JModel = $this->getModel();

        /** @var array $result */
        if ($result = $JModel->ordering($JInput->post))
        {
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($result);
            exit;
        }
    }

    /**
     * @since 12.2
     */
    public function save ()
    {
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /** @var JInput $JInput */
        $JInput = $this->input;

        /** @var JooIncarichiModelAttachment $JModel */
        $JModel = $this->getModel();

        /** @var array $result */
        if ($result = $JModel->save($JInput->post, $JInput->files))
        {
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($result);
            exit;
        }
    }

    /**
     * @since 12.2
     */
    public function state ()
    {
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /** @var JInput $JInput */
        $JInput = $this->input;

        /** @var JooIncarichiModelAttachment $JModel */
        $JModel = $this->getModel();

        /** @var array $result */
        if ($result = $JModel->state($JInput->post))
        {
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($result);
            exit;
        }
    }

    /**
     * @since 12.2
     */
    public function update ()
    {
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        /** @var JInput $JInput */
        $JInput = $this->input;

        /** @var JooIncarichiModelAttachment $JModel */
        $JModel = $this->getModel();

        /** @var array $result */
        if ($result = $JModel->update($JInput->post))
        {
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode($result);
            exit;
        }
    }
}

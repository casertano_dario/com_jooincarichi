<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_jooincarichi
 *
 * @author      Dario Casertano - www.casertano.name
 * @copyright   Copyright (c) 2016 Casertano Dario - All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported.
 * @version     16.5.31
 */
defined('_JEXEC') or die;

JLoader::register('JooIncarichiHelper', __DIR__ . '/helpers/jooincarichi.php');

/** @var JInput $JInput */
$JInput = JApplicationAdministrator::getInstance()->input;

/** @var JooIncarichiController $JController */
$JController = JControllerLegacy::getInstance('JooIncarichi');
$JController->execute($JInput->get('task', ''));
$JController->redirect();
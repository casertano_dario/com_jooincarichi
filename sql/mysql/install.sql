--                   O
--           /~~~|#|]|=\|---\__
--         |-=_____________  |\\ ,
--        I|_/,-.-.-.-.-,-.\_|='(   Dario Casertano
--          ( o )( o )( o )     \  www.casertano.name
--     __    `-'-'-'-'-`-'    _  \                      ___           _
--    / __\__ _ ___  ___ _ __| |_ __ _ _ __   ___      /   \__ _ _ __(C) ___
--   / /  / _` / __|/ _ \ '__| __/ _` | '_ \ / _ \    / /\ / _` | '__| |/ _ \
--  / /__| (_| \__ \  __/ |  | || (_| | | | | (_) |  / /_// (_| | |  | | (_) |
--  \____/\__,_|___/\___|_|   \__\__,_|_| |_|\___/  /___,' \__,_|_|  |_|\___/
-- ----------------------------------------------------------------------------
-- Copyright: (c) 2016 Casertano Dario - All rights reserved.
-- License: Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported.
-- Version: 16.5.31
-- ----------------------------------------------------------------------------
-- Database: `joomla`
-- ----------------------------------------------------------------------------

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

-- ----------------------------------------------------------------------------
-- Table structure for table `#__jooincarichi`
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `#__jooincarichi` (
    `id`                      INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `asset_id`                INT(10) UNSIGNED NOT NULL DEFAULT '0'
                              COMMENT 'FK to the #__assets table.',
    `title`                   VARCHAR(255)     NOT NULL DEFAULT '',
    `alias`                   VARCHAR(255)
                              CHARACTER SET utf8
                              COLLATE utf8_bin NOT NULL DEFAULT '',
    `introtext`               MEDIUMTEXT       NOT NULL,
    `fulltext`                MEDIUMTEXT       NOT NULL,
    `soggetto_percettore`     VARCHAR(255)     NOT NULL,
    `compenso_lordo_previsto` DECIMAL(12, 2) UNSIGNED   DEFAULT NULL
                              COMMENT 'Da compilare solo nel caso di soggetti beneficiari estranei alla Pubblica Amministrazione.',
    `compenso_lordo_erogato`  DECIMAL(12, 2)   NOT NULL,
    `data_inizio`             DATE             NOT NULL DEFAULT '0000-00-00',
    `data_fine`               DATE             NOT NULL DEFAULT '0000-00-00',
    `tipo_incarico`           TINYINT(3)       NOT NULL DEFAULT '0',
    `state`                   TINYINT(3)       NOT NULL DEFAULT '0',
    `created`                 DATETIME         NOT NULL DEFAULT '0000-00-00 00:00:00',
    `created_by`              INT(10) UNSIGNED NOT NULL DEFAULT '0',
    `created_by_alias`        VARCHAR(255)     NOT NULL DEFAULT '',
    `modified`                DATETIME         NOT NULL DEFAULT '0000-00-00 00:00:00',
    `modified_by`             INT(10) UNSIGNED NOT NULL DEFAULT '0',
    `checked_out`             INT(10) UNSIGNED NOT NULL DEFAULT '0',
    `checked_out_time`        DATETIME         NOT NULL DEFAULT '0000-00-00 00:00:00',
    `publish_up`              DATETIME         NOT NULL DEFAULT '0000-00-00 00:00:00',
    `attribs`                 VARCHAR(5120)    NOT NULL,
    `version`                 INT(10) UNSIGNED NOT NULL DEFAULT '1',
    `ordering`                INT(11)          NOT NULL DEFAULT '0',
    `metakey`                 TEXT             NOT NULL,
    `metadesc`                TEXT             NOT NULL,
    `access`                  INT(10) UNSIGNED NOT NULL DEFAULT '0',
    `hits`                    INT(10) UNSIGNED NOT NULL DEFAULT '0',
    `metadata`                TEXT             NOT NULL,
    `language`                CHAR(7)          NOT NULL
                              COMMENT 'The language code.',
    `xreference`              VARCHAR(50)      NOT NULL
                              COMMENT 'A reference to enable linkages to external data sets.',
    PRIMARY KEY (`id`),
    KEY `idx_access` (`access`),
    KEY `idx_checkout` (`checked_out`),
    KEY `idx_state` (`state`),
    KEY `idx_createdby` (`created_by`),
    KEY `idx_language` (`language`),
    KEY `idx_xreference` (`xreference`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8
    AUTO_INCREMENT = 0;

-- ----------------------------------------------------------------------------
-- Table structure for table `#__jooincarichi_attachments`
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `#__jooincarichi_attachments` (
    `id`          INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `item_id`     INT(10) UNSIGNED NOT NULL DEFAULT '0'
                  COMMENT 'FK to the #__jooincarichi table.',
    `media_path`  VARCHAR(255)     NOT NULL,
    `title`       VARCHAR(255)     NOT NULL,
    `alias`       VARCHAR(255)     NOT NULL DEFAULT '',
    `introtext`   MEDIUMTEXT       NOT NULL,
    `state`       TINYINT(3)       NOT NULL DEFAULT '0',
    `created`     DATETIME         NOT NULL DEFAULT '0000-00-00 00:00:00',
    `created_by`  INT(10) UNSIGNED NOT NULL DEFAULT '0',
    `modified`    DATETIME         NOT NULL DEFAULT '0000-00-00 00:00:00',
    `modified_by` INT(10) UNSIGNED NOT NULL DEFAULT '0',
    `ordering`    INT(11)          NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`),
    KEY `idx_state` (`state`),
    KEY `idx_createdby` (`created_by`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8
    AUTO_INCREMENT = 0;
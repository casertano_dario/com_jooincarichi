<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_jooincarichi
 *
 * @author      Dario Casertano - www.casertano.name
 * @copyright   Copyright (c) 2016 Casertano Dario - All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported.
 * @version     15.5.31
 */
defined('_JEXEC') or die;

/**
 * @package     ${NAMESPACE}
 *
 * @copyright   Copyright (c) 2016 Casertano Dario - All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported.
 */
class JooIncarichiController extends JControllerLegacy
{
    /**
     * @var string
     */
    protected $default_view = 'incarichi';
}

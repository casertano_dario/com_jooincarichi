<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_jooincarichi
 *
 * @author      Dario Casertano - www.casertano.name
 * @copyright   Copyright (C) 2016 Casertano Dario - All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported.
 * @version     16.6.6
 */
defined('_JEXEC') or die;

jimport('joomla.database.table');

class JTableJooIncarichiAttachments extends JTable
{
	/**
	* @inheritdoc
	*
	* @param JDatabaseDriver $JDbo
	*
	* @return $this
	 *
	 * @since 12.2
	*/
	public function __construct ( $JDbo )
	{
		parent::__construct('#__jooincarichi_attachments', 'id', $JDbo);
	}
}
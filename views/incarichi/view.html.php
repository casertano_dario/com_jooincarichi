<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_jooincarichi
 *
 * @author      Dario Casertano - www.casertano.name
 * @copyright   Copyright (c) 2016 Casertano Dario - All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported.
 * @version     16.5.31
 */
defined('_JEXEC') or die;

class JooIncarichiViewIncarichi extends JViewLegacy
{
	public $authors;
	public $incarichi;
	public $pagination;
	public $publishedOptions;
    public $sidebar;
	public $state;
    public $tipo_incarico;

    /**
	 * @inheritdoc
	 *
	 * @param string $tpl
	 *
	 * @since 12.2
	 */
	public function display ( $tpl = Null )
	{
		JToolbarHelper::title(JText::_('com_jooincarichi_ITEMS_TITLE'));

		$this->authors = $this->get('Authors');
		$this->incarichi = $this->get('Incarichi');
		$this->pagination = $this->get('Pagination');
		$this->publishedOptions = JHtml::_('jgrid.publishedOptions');
		unSet($this->publishedOptions[2]);
		$this->publishedOptions = array_values($this->publishedOptions);
		$this->state = $this->get('State');
		$this->tipo_incarico = $this->get('Tipo_incarico');

		if (count($errors = $this->get('Errors'))) throw new Exception(implode('<br />', $errors), 500);

			$this->addToolbar();
			$this->addSidebar();
			$this->sidebar = JHtmlSidebar::render();

		parent::display($tpl);
	}

	/**
	 * Toolbar
	 *
	 * @since 12.2
	 */
	protected function addToolbar ()
	{
		/**
		* @var JUser
		*/
		$JUser = JFactory::getUser();

        /** @var JObject $JState */
        $JState = $this->state;

		/**
		 * @var JObject
		 */
		$canDo = JooIncarichiHelper::getActions();

		if ($canDo->get('core.create') || count($JUser->getAuthorisedCategories('com_jooincarichi', 'core.create'))) JToolbarHelper::addNew('incarico.add');
		if ($canDo->get('core.edit') || $canDo->get('core.edit.own')) JToolbarHelper::editList('incarico.edit');
		if ($canDo->get('core.edit.state'))
		{
			JToolbarHelper::publish('incarichi.publish', 'JTOOLBAR_PUBLISH', true);
			JToolbarHelper::unpublish('incarichi.unpublish', 'JTOOLBAR_UNPUBLISH', true);
			JToolbarHelper::checkin('incarichi.checkin');
		}

		if (($JState->get('filter.published') == -2) && $canDo->get('core.delete')) JToolbarHelper::deleteList('', 'incarichi.delete', 'JTOOLBAR_EMPTY_TRASH');
		elseif ($canDo->get('core.edit.state')) JToolbarHelper::trash('incarichi.trash');

		if ($canDo->get('core.admin')) JToolbarHelper::preferences('com_jooincarichi');
	}

	/**
	 * Sidebar
	 *
	 * @since 12.2
	 */
	protected function addSidebar ()
	{
        /** @var JObject $JState */
        $JState = $this->state;

		JHtmlSidebar::setAction('index.php?option=com_jooincarichi&view=incarichi');

		JHtmlSidebar::addFilter
		(
			'- ' . JText::_('COM_JOOINCARICHI_FIELD_TIPO_INCARICO_LABEL') . ' -',
			'filter_tipo_incarico',
			JHtml::_('select.options', $this->tipo_incarico, 'value', 'text', $JState->get('filter.tipo_incarico'), true)
		);

		JHtmlSidebar::addFilter
		(
			JText::_('JOPTION_SELECT_PUBLISHED'),
			'filter_published',
			JHtml::_('select.options', $this->publishedOptions, 'value', 'text', $JState->get('filter.published'), true)
		);

		JHtmlSidebar::addFilter
		(
			JText::_('JOPTION_SELECT_ACCESS'),
			'filter_access',
			JHtml::_('select.options', JHtml::_('access.assetgroups'), 'value', 'text', $JState->get('filter.access'))
		);

		JHtmlSidebar::addFilter
		(
			JText::_('JOPTION_SELECT_AUTHOR'),
			'filter_author_id',
			JHtml::_('select.options', $this->authors, 'value', 'text', $JState->get('filter.author_id'))
		);

		JHtmlSidebar::addFilter
		(
			JText::_('JOPTION_SELECT_LANGUAGE'),
			'filter_language',
			JHtml::_('select.options', JHtml::_('contentlanguage.existing', true, true), 'value', 'text', $JState->get('filter.language'))
		);
	}

	/**
	 * Returns an array of fields the table can be sorted by
	 *
	 * @return array
	 *
	 * @since 12.2
	 */
	protected function getSortFields()
	{
		return array
		(
			'ji.ordering' => JText::_('JGRID_HEADING_ORDERING'),
			'ji.state' => JText::_('JSTATUS'),
			'ji.title' => JText::_('JGLOBAL_TITLE'),
			'ji.created_by' => JText::_('JAUTHOR'),
			'language' => JText::_('JGRID_HEADING_LANGUAGE'),
			'ji.created' => JText::_('JDATE'),
			'ji.id' => JText::_('JGRID_HEADING_ID'),
		);
	}
}
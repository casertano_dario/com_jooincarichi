<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_jooincarichi
 *
 * @author      Dario Casertano - www.casertano.name
 * @copyright   Copyright (C) 2013 Casertano Dario - All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported.
 * @version     13.8.30
 */
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('dropdown.init');
JHtml::_('formbehavior.chosen', 'select');

$JApp = JFactory::getApplication();

/** @var JPagination $JPagination */
$JPagination = $this->pagination;
$JState = $this->state;
$JUser = JFactory::getUser();

$listOrder = $this->escape($JState->get('list.ordering'));
$listDirn = $this->escape($JState->get('list.direction'));
$archived = ($JState->get('filter.published') == 2) ? true : false;
$trashed = ($JState->get('filter.published') == -2) ? true : false;
$saveOrder = $listOrder == 'a.ordering';

if ($saveOrder)
{
	$saveOrderingUrl = 'index.php?option=com_jooincarichi&task=incarichi.saveOrderAjax&tmpl=component';
	JHtml::_('sortablelist.sortable', 'incaricoList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}

$sortFields = $this->getSortFields();
$assoc = isSet($JApp->incarico_associations) ? $JApp->incarico_associations : 0;
?>
<script type="text/javascript">
Joomla.orderTable = function()
{
	table = document.getElementById('sortTable');
	direction = document.getElementById('directionTable');
	order = table.options[table.selectedIndex].value;
	if (order != '<?= $listOrder; ?>') dirn = 'asc';
	else dirn = direction.options[direction.selectedIndex].value;
	Joomla.tableOrdering(order, dirn, '');
}
</script>
<form action="<?= JRoute::_('index.php?option=com_jooincarichi&view=incarichi'); ?>" method="post" name="adminForm" id="adminForm">
<?
if (!empty( $this->sidebar))
{
?>
	<div id="j-sidebar-container" class="span2">
<?= $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?
}
else
{
?>
	<div id="j-main-container">
<?
}
?>
		<div id="filter-bar" class="btn-toolbar">
			<div class="filter-search btn-group pull-left">
				<label for="filter_search" class="element-invisible"><?= JText::_('COM_CONTENT_FILTER_SEARCH_DESC'); ?></label>
				<input type="text" name="filter_search" id="filter_search" placeholder="<?= JText::_('JSEARCH_FILTER'); ?>" value="<?= $this->escape($JState->get('filter.search')); ?>" class="hasTooltip" title="<?= JHtml::tooltipText('COM_CONTENT_FILTER_SEARCH_DESC'); ?>" />
			</div>
			<div class="btn-group pull-left hidden-phone">
				<button type="submit" class="btn hasTooltip" title="<?= JHtml::tooltipText('JSEARCH_FILTER_SUBMIT'); ?>"><i class="icon-search"></i></button>
				<button type="button" class="btn hasTooltip" title="<?= JHtml::tooltipText('JSEARCH_FILTER_CLEAR'); ?>" onclick="document.id('filter_search').value='';this.form.submit();"><i class="icon-remove"></i></button>
			</div>
			<div class="btn-group pull-right hidden-phone">
				<label for="limit" class="element-invisible"><?= JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC'); ?></label>
				<?= $this->pagination->getLimitBox(); ?>
			</div>
			<div class="btn-group pull-right hidden-phone">
				<label for="directionTable" class="element-invisible"><?= JText::_('JFIELD_ORDERING_DESC'); ?></label>
				<select name="directionTable" id="directionTable" class="input-medium" onchange="Joomla.orderTable()">
					<option value=""><?= JText::_('JFIELD_ORDERING_DESC'); ?></option>
					<option value="asc" <? if ($listDirn == 'asc') echo 'selected="selected"'; ?>><?= JText::_('JGLOBAL_ORDER_ASCENDING'); ?></option>
					<option value="desc" <? if ($listDirn == 'desc') echo 'selected="selected"'; ?>><?= JText::_('JGLOBAL_ORDER_DESCENDING');  ?></option>
				</select>
			</div>
			<div class="btn-group pull-right">
				<label for="sortTable" class="element-invisible"><?= JText::_('JGLOBAL_SORT_BY'); ?></label>
				<select name="sortTable" id="sortTable" class="input-medium" onchange="Joomla.orderTable()">
					<option value=""><?= JText::_('JGLOBAL_SORT_BY');?></option>
					<?= JHtml::_('select.options', $sortFields, 'value', 'text', $listOrder); ?>
				</select>
			</div>
		</div>
		<div class="clearfix"></div>
		<table class="table table-striped" id="incaricoList">
			<thead>
				<tr>
					<th width="1%" class="nowrap center hidden-phone"><?= JHtml::_('grid.sort', '<i class="icon-menu-2"></i>', 'ji.ordering', $listDirn, $listOrder, null, 'asc', 'JGRID_HEADING_ORDERING'); ?></th>
					<th width="1%" class="hidden-phone"><?= JHtml::_('grid.checkall'); ?></th>
					<th width="1%" style="min-width:55px" class="nowrap center"><?= JHtml::_('grid.sort', 'JSTATUS', 'ji.state', $listDirn, $listOrder); ?></th>
					<th><?= JHtml::_('grid.sort', 'JGLOBAL_TITLE', 'ji.title', $listDirn, $listOrder); ?></th>
					<th width="10%" class="nowrap hidden-phone"><?= JHtml::_('grid.sort',  'JGRID_HEADING_ACCESS', 'ji.access', $listDirn, $listOrder); ?></th>
					<th width="10%" class="nowrap hidden-phone"><?= JHtml::_('grid.sort',  'JAUTHOR', 'ji.created_by', $listDirn, $listOrder); ?></th>
					<th width="5%" class="nowrap hidden-phone"><?= JHtml::_('grid.sort', 'JGRID_HEADING_LANGUAGE', 'ji.language', $listDirn, $listOrder); ?></th>
					<th width="10%" class="nowrap hidden-phone"><?= JHtml::_('grid.sort', 'JDATE', 'ji.created', $listDirn, $listOrder); ?></th>
					<th width="10%"><?= JHtml::_('grid.sort', 'JGLOBAL_HITS', 'ji.hits', $listDirn, $listOrder); ?></th>
					<th width="1%" class="nowrap hidden-phone"><?= JHtml::_('grid.sort', 'JGRID_HEADING_ID', 'ji.id', $listDirn, $listOrder); ?></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="11"><?= $JPagination->getListFooter(); ?></td>
				</tr>
			</tfoot>
			<tbody>
<?
foreach ($this->incarichi as $i => $incarico)
{
	$incarico->max_ordering = 0;
	$ordering = ($listOrder == 'a.ordering');
	$canCreate = $JUser->authorise('core.create', "com_jooincarichi.category.{$incarico->catid}");
	$canEdit = $JUser->authorise('core.edit', "com_jooincarichi.incarico.{$incarico->id}");
	$canCheckin = $JUser->authorise('core.manage', 'com_checkin') || ($incarico->checked_out == $JUser->get('id')) || ($incarico->checked_out == 0);
	$canEditOwn = $JUser->authorise('core.edit.own', "com_jooincarichi.incarico.{$incarico->id}") && ($incarico->created_by == $JUser->get('id'));
	$canChange = $JUser->authorise('core.edit.state', "com_jooincarichi.incarico.{$incarico->id}") && $canCheckin;
?>
				<tr class="row<?= $i % 2; ?>" sortable-group-id="<?= $incarico->catid; ?>">
					<td class="order nowrap center hidden-phone">
						<span class="sortable-handler<?

						$iconClass = '';
						if (!$canChange) $iconClass = ' inactive';
						elseif (!$saveOrder) $iconClass = ' inactive tip-top hasTooltip" title="' . JHtml::tooltipText('JORDERINGDISABLED');

						echo $iconClass;

						?>"><i class="icon-menu"></i></span>
<?
	if ($canChange && $saveOrder)
	{
?>
						<input type="text" style="display:none" name="order[]" size="5" value="<?= $incarico->ordering; ?>" class="width-20 text-area-order " />
<?
	}
?>
					</td>
					<td class="center hidden-phone"><?= JHtml::_('grid.id', $i, $incarico->id); ?></td>
					<td class="center">
						<div class="btn-group">
							<?= JHtml::_('jgrid.published', $incarico->state, $i, 'incarichi.', $canChange, 'cb', $incarico->publish_up, $incarico->publish_down); ?>
						</div>
					</td>
					<td class="nowrap has-context">
						<div class="pull-left">
<?
	if ($incarico->checked_out) echo JHtml::_('jgrid.checkedout', $i, $incarico->editor, $incarico->checked_out_time, 'incarichi.', $canCheckin);
	if ($incarico->language == '*') $language = JText::alt('JALL', 'language');
	else $language = $incarico->language_title ? $this->escape($incarico->language_title) : JText::_('JUNDEFINED');
?>
<?
	if ($canEdit || $canEditOwn)
	{
?>
							<a href="<?= JRoute::_("index.php?option=com_jooincarichi&task=incarico.edit&id={$incarico->id}"); ?>" title="<?= JText::_('JACTION_EDIT'); ?>"><?= $this->escape($incarico->title); ?></a>
<?
	}
	else
	{
?>
							<span title="<?= JText::sprintf('JFIELD_ALIAS_LABEL', $this->escape($incarico->title)); ?>"><?= $this->escape($incarico->title); ?></span>
<?
	}
?>
							<div class="small break-word"><?= JText::_('COM_JOOINCARICHI_FIELD_TIPO_INCARICO_LABEL'); ?>: <?= $this->tipo_incarico[$incarico->tipo_incarico]; ?></div>
						</div>
						<div class="pull-left"><?

						JHtml::_('dropdown.edit', $incarico->id, 'incarico.');
						JHtml::_('dropdown.divider');

						if ($incarico->state) JHtml::_('dropdown.unpublish', 'cb' . $i, 'incarichi.');
						else JHtml::_('dropdown.publish', 'cb' . $i, 'incarichi.');

						if ($incarico->featured) JHtml::_('dropdown.unfeatured', 'cb' . $i, 'incarichi.');

						JHtml::_('dropdown.divider');

						if ($incarico->checked_out) JHtml::_('dropdown.checkin', 'cb' . $i, 'incarichi.');

						if ($trashed) JHtml::_('dropdown.untrash', 'cb' . $i, 'incarichi.');
						else JHtml::_('dropdown.trash', 'cb' . $i, 'incarichi.');

						echo JHtml::_('dropdown.render');

						?></div>
					</td>
					<td class="small hidden-phone"><?= $this->escape($incarico->access_level); ?></td>
					<td class="small hidden-phone">
						<a href="<?= JRoute::_('index.php?option=com_users&task=user.edit&id='.(int) $incarico->created_by); ?>" title="<?= JText::_('JAUTHOR'); ?>"><?= $this->escape($incarico->author_name); ?></a>
<?
	if ($incarico->created_by_alias)
	{
?>
						<p class="smallsub"> <?= JText::sprintf('JGLOBAL_LIST_ALIAS', $this->escape($incarico->created_by_alias)); ?></p>
<?
	}
?>
					</td>
					<td class="small hidden-phone"><?

					if ($incarico->language == '*') echo JText::alt('JALL', 'language');
					else echo $incarico->language_title ? $this->escape($incarico->language_title) : JText::_('JUNDEFINED');

					?></td>
					<td class="nowrap small hidden-phone"><?= JHtml::_('date', $incarico->created, JText::_('DATE_FORMAT_LC4')); ?></td>
					<td class="center"><?= (int) $incarico->hits; ?></td>
					<td class="center hidden-phone"><?= (int) $incarico->id; ?></td>
				</tr>
<?
}
?>
			</tbody>
		</table>
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?= $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?= $listDirn; ?>" />
		<?= JHtml::_('form.token') . PHP_EOL; ?>
	</div>
</form>

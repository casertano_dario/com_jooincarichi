<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_jooincarichi
 *
 * @author      Dario Casertano - www.casertano.name
 * @copyright   Copyright (c) 2016 Casertano Dario - All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported.
 *
 * @version     16.6.6
 */
defined('_JEXEC') or die;

class JooIncarichiViewIncarico extends JViewLegacy
{
	public $attachments;
    public $canDo;
	public $form;
	public $incarico;
	public $state;

    /**
	 * @inheritdoc
	 *
	 * @param string $tpl
	 *
	 * @since 12.2
	 */
	public function display ( $tpl = Null )
	{
        $this->attachments = $this->get('Attachments');
		$this->form = $this->get('Form');
		$this->incarico = $this->get('Incarico');
        $this->canDo = JooIncarichiHelper::getActions($this->incarico->id);
		$this->state = $this->get('State');

		if (count($errors = $this->get('Errors'))) throw new Exception(implode(PHP_EOL, $errors), 500);

		$this->addToolbar();
		if ($this->incarico->id)
		{
			JFactory::getDocument()->addStyleSheet('components/com_jooincarichi/views/incarico/tmpl/css/com_jooincarichi.css');
			JFactory::getDocument()->addScript('components/com_jooincarichi/views/incarico/tmpl/js/FileAPI/2.0.20/FileAPI.min.js');
		}

		parent::display($tpl);
	}

	/**
	 * Toolbar
	 *
	 * @since 12.2
	 */
	protected function addToolbar ()
	{
		JFactory::getApplication()->input->set('hidemainmenu', true);

        /** @var JUser $JUser */
		$JUser = JFactory::getUser();

		$incarico = $this->incarico;
		$isNew = ($incarico->id == 0);
		$checkedOut	= !(!$incarico->checked_out || ($incarico->checked_out == $JUser->get('id')));

		JToolbarHelper::title(JText::_('COM_JOOINCARICHI_PAGE_' . ($checkedOut ? 'VIEW_ITEM' : ($isNew ? 'ADD_ITEM' : 'EDIT_ITEM'))));

		if ($isNew && count($JUser->getAuthorisedCategories('com_jooincarichi', 'core.create'))) JToolbarHelper::apply('incarico.apply');
		else
		{
			if (!$checkedOut)
			{
				if
				(
                    $this->canDo->get('core.edit') ||
					(
                        $this->canDo->get('core.edit.own') &&
						($incarico->created_by == $JUser->get('id'))
					)
				)
				{
					JToolbarHelper::apply('incarico.apply');
					JToolbarHelper::save('incarico.save');

					if ($this->canDo->get('core.create')) JToolbarHelper::save2new('incarico.save2new');
				}
			}
		}

		JToolbarHelper::cancel('incarico.cancel');
	}
}

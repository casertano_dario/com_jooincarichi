<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_jooproducts
 *
 * @author      Dario Casertano - www.casertano.name
 * @copyright   Copyright (C) 2013 Casertano Dario - All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported.
 * @version     13.9.5
 */
defined('_JEXEC') or die;

echo JLayoutHelper::render('joomla.edit.metadata', $this);

<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_jooincarichi
 *
 * @author      Dario Casertano - www.casertano.name
 * @copyright   Copyright (c) 2016 Casertano Dario - All rights reserved.
 * @license     Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported.
 *
 * @version     16.6.6
 */
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('dropdown.init');

JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
JHtml::_('formbehavior.chosen', 'select');

/** @var JApplicationAdministrator $JApp */
$JApp = JFactory::getApplication();

/** @var JForm $JForm */
$JForm = $this->form;

/** @var JInput $Input */
$Input = $JApp->input;

/** @var \Joomla\Registry\Registry $JParams */
$JParams = $this->state->get('params');
$params = $JParams->toArray();

$attachments = $this->attachments;
$incarico = $this->incarico;
$editoroptions = isSet($params['show_publishing_options']);

$assoc = isSet($JApp->item_associations) ? $JApp->item_associations : 0;

if (!$editoroptions)
{
	$params['show_publishing_options'] = '1';
	$params['show_article_options'] = '1';
	$params['show_urls_images_backend'] = '0';
	$params['show_urls_images_frontend'] = '0';
}

if (!empty($incarico->attribs['show_publishing_options'])) $params['show_publishing_options'] = $incarico->attribs['show_publishing_options'];
if (!empty($incarico->attribs['show_article_options'])) $params['show_article_options'] = $incarico->attribs['show_article_options'];
if (!empty($incarico->attribs['show_urls_images_backend'])) $params['show_urls_images_backend'] = $incarico->attribs['show_urls_images_backend'];

JHtml::_('sortablelist.sortable', 'itemList', 'adminForm', strtolower($listDirn), 'index.php?option=com_jooincarichi&task=attachment.ordering');
?>
<script>
Joomla.submitbutton = function ( task )
{
	if (task == 'incarico.cancel' || document.formvalidator.isValid(document.id('item-form')))
	{
		<?= $JForm->getField('itemtext')->save() . PHP_EOL; ?>
		Joomla.submitform(task, document.getElementById('item-form'));
	}
};
jQuery(function ( $ )
{
	/**
	 * Attachs control
	 */
	(function ( el )
	{
		var $mm_container = $(el);
		var $div = $('<div class="gallery"><b><?= JText::_('COM_JOOINCARICHI_FIELDSET_ATTACHMENTS_DRAGAREA'); ?></b></div>');
		var $table = $('<table>').addClass('table table-striped');
        var $thead = $('<thead>');
        var $tr = $('<tr>');
        var $th1 = $('<th width="1%" class="nowrap center hidden-phone"><i class="icon-menu-2"></i></th>');
        var $th2 = $('<th><?= JText::_('COM_JOOINCARICHI_FIELDSET_ATTACHMENTS_TH_ATTACH'); ?></th>');
        var $th3 = $('<th><?= JText::_('COM_JOOINCARICHI_FIELDSET_ATTACHMENTS_TH_TITLE'); ?></th>');
        var $th4 = $('<th width="1%"></th>');
        var $th5 = $('<th width="1%"></th>');
        var $th6 = $('<th width="1%"></th>');
        $tr.append($th1, $th2, $th3, $th4, $th5, $th6);
        $thead.append($tr);

        var $tbody = $('<tbody id="tbody-attachs">');
		$table.append($thead, $tbody);

		var $onDrop = function ( e )
		{
			var $uploaded = [];
			var $uploadedStep = 0;

			FileAPI.upload(
			{
				url: 'index.php?option=com_jooincarichi&task=attachment.save',
				data:
				{
					'created_by': parseInt(<?= JFactory::getUser()->get('id', 0); ?>),
					'item_id': parseInt(<?= $incarico->get('id', 0); ?>),
					'<?= JSession::getFormToken(); ?>': 1
				},
				files:
				{
					upload: FileAPI.filter(FileAPI.getFiles(e), function ( file )
					{
						return /^application\/(download|zip|msword|vnd\.openxmlformats-officedocument\.wordprocessingml\.document|vnd\.ms-excel|vnd\.openxmlformats-officedocument\.spreadsheetml\.sheet|vnd\.ms-powerpoint|vnd\.openxmlformats-officedocument\.presentationml\.slideshow|txt|rtf|pdf)$/i.test(file.type);
					})
				},
				prepare: function ( file, options )
				{
					$uploaded.push(file.name);

					FileAPI.readAsDataURL(file, function ( e )
					{
						switch ( e.type )
						{
							case 'load':

								if (!$('#tbody-attachs').length)
								{
									$mm_container.find('> .gallery').remove();
									$mm_container.append($table);
								}

								var $mm_gallery = $('#tbody-attachs');

								var $tr = $('<tr>').attr('data-name', e.target.name);
                                var $td1 = $('<td class="order nowrap center hidden-phone"><span class="sortable-handler" style="cursor:move"><i class="icon-menu"></i></span></td>');
                                var $td2 = $('<td>' + e.target.name + '</td>');
                                var $td3 = $('<td>');
                                var $input = $('<input type="text" placeholder="<?= JText::_('COM_JOOINCARICHI_FIELDSET_ATTACHMENTS_FIELD_TITLE_LABEL'); ?>" name="attachments_title">').hide();
                                var $progress = $('<div class="progress"><span></span></div>');
                                $td3.append($input, $progress);

                                var $td4 = $('<td>');
                                var $save = $('<a rel="nofollow" title="<?= JText::_('COM_JOOINCARICHI_FIELDSET_MM_IMAGES_BTN_SAVE_LABEL'); ?>" class="btn btn-small btn-success attachment_save" href="javascript:;"><span class="icon-apply"></span></a>').css('visibility:hidden');
                                $td4.append($save);

                                var $td5 = $('<td>');
                                var $state = $('<a rel="nofollow" title="<?= JText::_('COM_JOOINCARICHI_FIELDSET_ATTACHMENTS_BTN_STATE_LABEL'); ?>" class="btn btn-small btn-danger attachment_state" href="javascript:;"><i class="icon-cancel"></i></a>');
                                $td5.append($state);

                                var $td6 = $('<td>');
                                var $trash = $('<a rel="nofollow" title="<?= JText::_('COM_JOOINCARICHI_FIELDSET_ATTACHMENTS_BTN_TRASH_LABEL'); ?>" class="btn btn-small btn-inverse attachment_trash" href="javascript:;"><i class="icon-trash"></i></a>');
                                $td6.append($trash);
								$tr.append($td1, $td2, $td3, $td4, $td5, $td6).appendTo($mm_gallery);

                                break;
						}
					});
				},
				fileprogress: function ( e )
				{
					var $tr = $('tr[data-name="' + $uploaded[$uploadedStep] + '"]');
						$tr.find('div.progress > span').stop().animate({'width': (e.loaded / e.total * 100) + '%'});
				},
				filecomplete: function ( err, xhr )
				{
					var $tr = $('tr[data-name="' + $uploaded[$uploadedStep] + '"]');
						$tr.find('div.progress > span').stop().animate({'width': '100%'},
						{
							complete: function ()
							{
								var $resp = $.parseJSON(xhr.responseText);

                                switch ($resp.returnCode)
                                {
                                    case 200:

                                        $tr.data('id', $resp.id);
                                        $tr.data('name', $resp.file_name);
                                        $($tr.find('td')[1]).html($resp.file_name);
                                        $tr.find('div.progress').remove();
                                        $tr.find('input[type="text"]').show().val($resp.title);

                                        $('#tbody-attachs').sortable(
                                            {
                                                handle: $('.sortable-handler')
                                            });

                                        break;

                                    default:

                                        alert($resp.returnMessage);
                                }

							}
						});

					$uploadedStep++;
				}
			});
		};

		$mm_container.find('button[type="button"].add').on('click', function ()
		{
			$mm_container.find('input[type="file"][name="up_attach"]').click();
		});
		$mm_container.find('input[type="file"][name="up_attach"]').on('change', $onDrop);

		$('body').on('click', '.attachment_state', function ()
		{
			var _this = $(this);
			var $figure = _this.parent().parent();

			$.post('index.php?option=com_jooincarichi&task=attachment.state',
			{
				'modified_by': parseInt(<?= JFactory::getUser()->get('id', 0); ?>),
				'id': $figure.data('id'),
				'state': _this.hasClass('btn-warning') ? 0 : 1,
                '<?= JSession::getFormToken(); ?>': 1
			}).done(function ()
			{
				var $div = $figure.find('> div.img');
				if (!_this.hasClass('btn-danger'))
				{
					_this.addClass('btn-danger');
					$div.append('<div class="disable"></div>');
				}
				else
				{
					_this.removeClass('btn-danger');
					$div.find('> div.disable').remove();
				}
			});
		}).on('click', '.attachment_trash', function ()
		{
			var $tr = $(this).parent().parent();
			var $tbody = $('#tbody-attachs');

			$.post('index.php?option=com_jooincarichi&task=attachment.delete',
			{
				'item_id': parseInt(<?= $incarico->get('id', 0); ?>),
				'id': $tr.data('id'),
				'name': $tr.data('name'),
                '<?= JSession::getFormToken(); ?>': 1
			}).done(function ()
			{
				$tr.remove();

				if (!$tbody.find('> tr').length)
				{
					$tbody.parent().remove();
					$mm_container.append($div);
				}
			});
		}).on('click', '.attachment_save', function ()
		{
			var _this = $(this);
			var $tr = _this.parent().parent();
			var $input = $tr.find('input[type="text"]');
			var $textarea = $tr.find('textarea');

			if ($.trim($input.val())) $.post('index.php?option=com_jooincarichi&task=attachment.update',
			{
				'modified_by': parseInt(<?= JFactory::getUser()->get('id', 0); ?>),
				'id': $tr.data('id'),
				'title': $input.val(),
				'introtext': $textarea.val(),
                '<?= JSession::getFormToken(); ?>': 1
			}).done(function ()
			{
                _this.css('visibility', 'hidden');
			});
			else
			{
				$input.val($input.data('value'));
                _this.css('visibility', 'hidden');
			}
		}).on('keyup', 'input[name="attachments_title"]', function ()
		{
			var $save = $(this).parent().parent().find('a.attachment_save');
			$save.css('visibility', 'visible');
		});

		$('#tbody-attachs').sortable(
		{
			axis: 'y',
			stop: function ( e, ui )
			{
				var $ids = new Array();
				var $tr = $mm_container.find('tr');

				for (var i = 0, ii = $tr.length; i != ii; i++)
					if ($($tr[i]).data('id'))
						$ids.push($($tr[i]).data('id'));

				$.post('index.php?option=com_jooincarichi&task=attachment.ordering',
				{
					'modified_by': parseInt(<?= JFactory::getUser()->get('id', 0); ?>),
					'item_id': parseInt(<?= $incarico->get('id', 0); ?>),
					'ids': $ids
				});
			},
			connectWith: 'tr',
			cursor: 'move',
			handle: $('.sortable-handler'),
			helper: function ( e, ui )
			{
				$(ui).css('left', 0);

				ui.children().each(function ()
				{
					$(this).width($(this).width());
				});

				return ui;
			},
			placeholder: 'dnd-list-highlight dndlist-place-holder'
		});

		FileAPI.event.dnd($mm_container[0], function ( over, e )
		{
			var $mm_gallery = $(this).find('> .gallery');
			if ($mm_gallery.hasClass('empty')) $mm_gallery.css('background-color', over ? '#ffffbf' : '#fff');
			else $(this).css('background-color', over ? '#ffffbf' : '#fff');
		}, $onDrop);
	})('#attachments');
});
</script>
<form action="<?= JRoute::_('index.php?option=com_jooincarichi&layout=edit&id=' . $incarico->get('id', 0)); ?>" method="post" name="adminForm" id="item-form" class="form-validate">
    <div class="form-horizontal">
        <?= JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

        <?= JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_JOOINCARICHI_ITEM_INFORMATIONS', true)); ?>
	    <div class="row-fluid">
		    <div class="span10">
			    <fieldset class="adminform">
<?
        foreach (array
                 (
                     'title',
                     'tipo_incarico',
                     'soggetto_percettore',
                     'compenso_lordo_previsto',
                     'compenso_lordo_erogato',
                     'data_inizio',
                     'data_fine',
                     'itemtext',
                 ) as $key)
        {
?>
                    <div class="control-group">
                        <div class="control-label"><?= $JForm->getLabel($key); ?></div>
                        <div class="controls"><?= $JForm->getInput($key); ?></div>
                    </div>
<?
        }
?>
                </fieldset>
            </div>
            <div class="span2"><?= JLayoutHelper::render('joomla.edit.global', $this); ?></div>
        </div>
        <?= JHtml::_('bootstrap.endTab'); ?>
<?
		if ($incarico->get('id', false))
		{
			echo JHtml::_('bootstrap.addTab', 'myTab', 'attachments', JText::_('COM_JOOINCARICHI_FIELDSET_ATTACHMENTS', true));
?>
			<div class="cockpit">
				<button type="button" class="btn btn-info add"><i class="icon-flag-2"></i>&nbsp;&nbsp;<?= JText::_('COM_JOOINCARICHI_FIELDSET_ATTACHMENTS_BTN_ADD'); ?></button>
				<input type="file" name="up_attach" accept="<?

				echo implode(',', array
				(
					'application/zip',
					'application/msword',
					'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
					'application/vnd.ms-excel',
					'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
					'application/vnd.ms-powerpoint',
					'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
					'application/txt',
					'application/rtf',
					'application/pdf',
				));

				?>" multiple style="visibility:hidden">
			</div>
<?
				if (empty($attachments))
				{
?>
			<div class="gallery"><b><?= JText::_('COM_JOOINCARICHI_FIELDSET_ATTACHMENTS_DRAGAREA'); ?></b></div>
<?
				}
				else
				{
?>
			<table class="table table-striped">
				<thead>
					<tr>
						<th width="1%" class="nowrap center hidden-phone"><i class="icon-menu-2"></i></th>
						<th width="46%"><?= JText::_('COM_JOOINCARICHI_FIELDSET_ATTACHMENTS_TH_ATTACH'); ?></th>
						<th width="50%"><?= JText::_('COM_JOOINCARICHI_FIELDSET_ATTACHMENTS_TH_TITLE'); ?></th>
						<th width="1%"></th>
						<th width="1%"></th>
						<th width="1%"></th>
					</tr>
				</thead>
				<tbody id="tbody-attachs">
<?
					foreach ($attachments as $attachment)
					{
?>
					<tr data-name="<?= $attachment->media_path; ?>" data-id="<?= $attachment->id; ?>">
						<td class="order nowrap center hidden-phone"><span class="sortable-handler" style="cursor:move"><i class="icon-menu"></i></span></td>
						<td><?= $attachment->media_path; ?></td>
						<td><input type="text" placeholder="<?= JText::_('COM_JOOINCARICHI_FIELDSET_ATTACHMENTS_FIELD_TITLE_LABEL'); ?>" name="attachments_title" data-value="<?= htmlentities($attachment->title, ENT_QUOTES, 'UTF-8'); ?>" value="<?= htmlentities($attachment->title, ENT_QUOTES, 'UTF-8'); ?>" class="input-xlarge required"></td>
						<td><a rel="nofollow" title="<?= JText::_('COM_JOOINCARICHI_FIELDSET_ATTACHMENTS_BTN_SAVE_LABEL'); ?>" class="btn btn-small btn-success attachment_save" href="javascript:;" style="visibility:hidden"><span class="icon-apply"></span></a></td>
						<td><a rel="nofollow" title="<?= JText::_('COM_JOOINCARICHI_FIELDSET_ATTACHMENTS_BTN_STATE_LABEL'); ?>" class="btn btn-small <?= !$attachment->state ? 'btn-danger ' : ''; ?>attachment_state" href="javascript:;"><i class="icon-cancel"></i></a></td>
						<td><a rel="nofollow" title="<?= JText::_('COM_JOOINCARICHI_FIELDSET_ATTACHMENTS_BTN_TRASH_LABEL'); ?>" class="btn btn-small btn-inverse attachment_trash" href="javascript:;"><i class="icon-trash"></i></a></td>
					</tr>
<?
					}
?>
				</tbody>
			</table>
<?
				}
			echo JHtml::_('bootstrap.endTab');
        }

    if ($params['show_publishing_options'] || (empty($params['show_publishing_options']) && !empty($editoroptions)))
    {
        echo JHtml::_('bootstrap.addTab', 'myTab', 'publishing', JText::_('COM_JOOINCARICHI_FIELDSET_PUBLISHING', true));
?>
        <div class="row-fluid">
            <div class="span6">
                <div class="control-group">
                    <div class="control-label"><?= $JForm->getLabel('alias'); ?></div>
                    <div class="controls"><?= $JForm->getInput('alias'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?= $JForm->getLabel('id'); ?></div>
                    <div class="controls"><?= $JForm->getInput('id'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?= $JForm->getLabel('created_by'); ?></div>
                    <div class="controls"><?= $JForm->getInput('created_by'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?= $JForm->getLabel('created_by_alias'); ?></div>
                    <div class="controls"><?= $JForm->getInput('created_by_alias'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?= $JForm->getLabel('created'); ?></div>
                    <div class="controls"><?= $JForm->getInput('created'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?= $JForm->getLabel('publish_up'); ?></div>
                    <div class="controls"><?= $JForm->getInput('publish_up'); ?></div>
                </div>
<?
        if ($incarico->modified_by)
        {
?>
                <div class="control-group">
                    <div class="control-label"><?= $JForm->getLabel('modified_by'); ?></div>
                    <div class="controls"><?= $JForm->getInput('modified_by'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?= $JForm->getLabel('modified'); ?></div>
                    <div class="controls"><?= $JForm->getInput('modified'); ?></div>
                </div>
<?
        }

        if ($incarico->version)
        {
?>
                <div class="control-group">
                    <div class="control-label"><?= $JForm->getLabel('version'); ?></div>
                    <div class="controls"><?= $JForm->getInput('version'); ?></div>
                </div>
<?
        }

        if ($incarico->hits)
        {
?>
                <div class="control-group">
                    <div class="control-label"><?= $JForm->getLabel('hits'); ?></div>
                    <div class="controls"><?= $JForm->getInput('hits'); ?></div>
                </div>
<?
        }
?>
            </div>
        </div>
<?
        echo JHtml::_('bootstrap.endTab');
    }

    if ($params['show_article_options'] || (empty($params['show_article_options']) && !empty($editoroptions)))
    {
        $fieldSets = $JForm->getFieldsets('attribs');

        foreach ($fieldSets as $name => $fieldSet)
        {
            if (!in_array($name, array('editorConfig', 'basic-limited'))) echo JHtml::_('bootstrap.addTab', 'myTab', "attrib-{$name}", JText::_($fieldSet->label, true));
            {
                if ($params['show_article_options'] || (empty($params['show_article_options']) && !empty($editoroptions)))
                {
                    if (!in_array($name, array('editorConfig', 'basic-limited')))
                    {
                        if (isSet($fieldSet->description) && trim($fieldSet->description))
                        {
?>
			<p class="tip"><?= $this->escape(JText::_($fieldSet->description));?></p>
<?
                        }

                        foreach ($JForm->getFieldset($name) as $field)
                        {
?>
        <div class="control-group">
            <div class="control-label"><?= $field->label; ?></div>
            <div class="controls"><?= $field->input; ?></div>
        </div>
<?
                        }
                    }
                    elseif ($name == 'basic-limited') foreach ($JForm->getFieldset('basic-limited') as $field) echo $field->input;
                }
            }
            if (!in_array($name, array('editorConfig', 'basic-limited'))) echo JHtml::_('bootstrap.endTab');
        }
    }

        echo JHtml::_('bootstrap.addTab', 'myTab', 'metadata', JText::_('JGLOBAL_FIELDSET_METADATA_OPTIONS', true));
        echo $this->loadTemplate('metadata');
		echo JHtml::_('bootstrap.endTab');

    if ($this->canDo->get('core.admin'))
    {
        echo JHtml::_('bootstrap.addTab', 'myTab', 'permissions', JText::_('COM_JOOINCARICHI_FIELDSET_RULES', true));
        echo $JForm->getInput('rules');
        echo JHtml::_('bootstrap.endTab');
    }

	echo JHtml::_('bootstrap.endTabSet');
?>
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="return" value="<?= $Input->getCmd('return');?>" />
		<?= JHtml::_('form.token') . PHP_EOL; ?>
	</div>
</form>
